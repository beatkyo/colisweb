package colisweb.transporter

import colisweb.transporter.LicenceType.A
import colisweb.transporter.LicenceType.B
import colisweb.transporter.LicenceType.C
import colisweb.transporter.TransporterService.DuplicateSiretError
import colisweb.transporter.TransporterService.EmptyNameError
import org.scalatest.EitherValues
import org.scalatest.FlatSpecLike
import org.scalatest.Matchers
import org.scalatest.concurrent.ScalaFutures

import scala.concurrent.ExecutionContext.Implicits.global

class TransporterServiceImplSpec extends FlatSpecLike with Matchers with ScalaFutures with EitherValues {

  behavior of "TransporterServiceImpl"

  it should "createTransporter" in {
    val repository = new TransporterRepositoryStub
    val service = new TransporterServiceImpl(repository)

    val transporter = Transporter("S42", "moveit", "CDX-555" :: "59320" :: "PB-44" :: Nil, Nil)

    repository.transporters shouldBe empty
    service.createTransporter(transporter).futureValue shouldBe empty
    repository.transporters should contain only transporter
  }

  it should "fail createTransporter" in {
    val repository = new TransporterRepositoryStub
    val service = new TransporterServiceImpl(repository)

    val tr0 = Transporter("S123", "moveit", Nil, Nil)

    repository.transporters shouldBe empty

    service.createTransporter(tr0).futureValue shouldBe empty
    repository.transporters should contain(tr0)

    val tr1 = tr0 copy (name = "")

    service.createTransporter(tr1).futureValue should contain allOf (
      DuplicateSiretError(tr1),
      EmptyNameError(tr1)
    )

    repository.transporters should contain only tr0
  }

  it should "getTransportersByLicence" in {
    val repository = new TransporterRepositoryStub
    val service = new TransporterServiceImpl(repository)

    val trA = Transporter("S-A", "tr-A", Nil, A :: Nil)
    val trB = Transporter("S-B", "tr-B", Nil, B :: Nil)
    val trAB = Transporter("S-AB", "tr-AB", Nil, A :: B :: Nil)

    repository.transporters shouldBe empty

    service.createTransporter(trA).futureValue shouldBe empty
    service.createTransporter(trB).futureValue shouldBe empty
    service.createTransporter(trAB).futureValue shouldBe empty

    repository.transporters should contain allOf (trA, trB, trAB)

    service.listTransporters().futureValue should contain theSameElementsAs List(trA, trB, trAB)
    service.listTransporters(Some(A)).futureValue should contain theSameElementsAs List(trA, trAB)
    service.listTransporters(Some(B)).futureValue should contain theSameElementsAs List(trB, trAB)
    service.listTransporters(Some(C)).futureValue shouldBe empty
  }

}
