package colisweb.transporter
import scala.concurrent.Future
import scala.concurrent.Future.successful

/**
  * For tests only. Synchronized.
  */
class TransporterRepositoryStub extends TransporterRepository {

  private var _transporters: List[Transporter] = Nil

  def transporters: List[Transporter] = _transporters

  def findTransporterBySIRET(siret: String): Future[Option[Transporter]] = {
    successful(_transporters.find(_.siret.toLowerCase == siret.toLowerCase))
  }

  def listTransportersByLicence(license: LicenceType): Future[List[Transporter]] = {
    successful(_transporters.filter(_.licences contains license))
  }

  def listTransporters: Future[List[Transporter]] = {
    successful(_transporters)
  }

  def createTransporter(transporter: Transporter): Future[Unit] = {
    this.synchronized { _transporters ::= transporter }

    successful(())
  }

}
