package colisweb.transporter

import colisweb.transporter.TransporterService.CreateTransporterError

import scala.concurrent.Future

trait TransporterService {
  def listTransporters(licenceFilter: Option[LicenceType] = None): Future[List[Transporter]]

  def createTransporter(transporter: Transporter): Future[List[CreateTransporterError]]
}

object TransporterService {
  sealed trait Error { val message: String }

  sealed trait CreateTransporterError extends Error
  sealed case class EmptyNameError(transporter: Transporter) extends CreateTransporterError { val message = s"Empty name: $transporter" }
  sealed case class DuplicateSiretError(transporter: Transporter) extends CreateTransporterError { val message = s"Duplicate SIRET: $transporter" }
}
