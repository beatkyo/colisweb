package colisweb.transporter
import scala.concurrent.Future

trait TransporterRepository {
  def findTransporterBySIRET(siret: String): Future[Option[Transporter]]

  def listTransportersByLicence(license: LicenceType): Future[List[Transporter]]

  def listTransporters: Future[List[Transporter]]

  def createTransporter(transporter: Transporter): Future[Unit]
}
