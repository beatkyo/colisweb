package colisweb.transporter

import colisweb.transporter.TransporterService.CreateTransporterError
import colisweb.transporter.TransporterService.DuplicateSiretError
import colisweb.transporter.TransporterService.EmptyNameError

import scala.concurrent.ExecutionContext
import scala.concurrent.Future
import scala.concurrent.Future.sequence
import scala.concurrent.Future.successful

class TransporterServiceImpl(repository: TransporterRepository)(implicit ec: ExecutionContext) extends TransporterService {

  def listTransporters(licenceFilter: Option[LicenceType] = None): Future[List[Transporter]] = {
    licenceFilter match {
      case Some(licence) => repository.listTransportersByLicence(licence)
      case None          => repository.listTransporters
    }
  }

  def createTransporter(transporter: Transporter): Future[List[CreateTransporterError]] = {
    val validation = {
      val errors = sequence {
        List(
          repository.findTransporterBySIRET(transporter.siret) map (_ map (_ => DuplicateSiretError(transporter))),
          successful { if (transporter.name.trim.isEmpty) Some(EmptyNameError(transporter)) else None }
        )
      }

      errors map (_.flatten)
    }

    validation flatMap {
      case Nil    => repository.createTransporter(transporter) map (_ => Nil)
      case errors => successful { errors }
    }
  }

}
