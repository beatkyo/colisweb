package colisweb.transporter

sealed trait LicenceType

object LicenceType {
  object A extends LicenceType
  object B extends LicenceType
  object C extends LicenceType

  implicit def fromString: String => LicenceType = {
    case "A" => LicenceType.A
    case "B" => LicenceType.B
    case "C" => LicenceType.C
  }
}
