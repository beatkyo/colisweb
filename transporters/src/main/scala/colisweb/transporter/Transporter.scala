package colisweb.transporter

case class Transporter(siret: String, name: String, postalCodes: List[String], licences: List[LicenceType])
