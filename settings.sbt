import sbt.Keys.libraryDependencies

inThisBuild {
  Seq(
    organization := "colisweb",

    scalaVersion := "2.12.7",

    scalafmtVersion := "1.5.1",
    scalafmtOnCompile := true,
    scalafmtOnCompile in Sbt := false,

    javacOptions in Compile in compile ++= Seq("-source", "1.8", "-target", "1.8"),
    javacOptions in Test in compile ++= Seq("-source", "1.8", "-target", "1.8"),

    conflictManager := ConflictManager.latestRevision,

    resolvers += Resolver.sonatypeRepo("releases"),
    resolvers += Resolver.sonatypeRepo("snapshots"),
    resolvers += Resolver.typesafeRepo("releases"),
    resolvers += Resolver.sbtPluginRepo("releases"),

    // GLOBAL TEST DEPENDENCIES
    libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.5" % "test",
    libraryDependencies += "org.scalacheck" %% "scalacheck" % "1.14.0" % "test"
  )
}