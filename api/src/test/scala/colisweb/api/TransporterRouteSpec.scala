package colisweb.api

import akka.http.scaladsl.model.StatusCodes.BadRequest
import akka.http.scaladsl.model.StatusCodes.OK
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.testkit.ScalatestRouteTest
import org.scalatest.EitherValues
import org.scalatest.FlatSpecLike
import org.scalatest.Matchers

class TransporterRouteSpec extends FlatSpecLike with ScalatestRouteTest with Matchers with JsonSupport with ModelMapper with EitherValues {

  behavior of "TransportRoute"

  it should "createTransporter" in {
    val transporterService = new TransporterServiceStub
    val carrierService = new CarrierServiceStub
    val route: Route = new TransporterRoute(transporterService, carrierService)

    val carriers = {
      List(0, 1, 2, 3) map { i =>
        CarrierInfo(s"carr-$i", i, List("A", "B", "C"))
      }
    }

    val transporter = TransporterInfo("moveit", "S1234", List("A59", "Z12", "X44"), carriers)

    transporterService.transporters shouldBe empty
    carrierService.carriers shouldBe empty

    Post("/transporters", transporter) ~> route ~> check {
      status shouldBe OK
      transporterService.transporters should contain(toDomain(transporter))
      carrierService.carriers should contain allElementsOf { carriers map (toDomain(_, transporter.siret)) }
    }
  }

  it should "fail createTransporter" in {

    {
      info { "empty carriers" }
      val transporterService = new TransporterServiceStub
      val carrierService = new CarrierServiceStub
      val route: Route = new TransporterRoute(transporterService, carrierService)

      val transporter = TransporterInfo("moveit", "S1234", List("A59", "Z12", "X44"), Nil)

      transporterService.transporters shouldBe empty
      carrierService.carriers shouldBe empty

      Post("/transporters", transporter) ~> route ~> check {
        status shouldBe BadRequest
        responseAs[String].trim should not be empty
        transporterService.transporters shouldBe empty
        carrierService.carriers shouldBe empty
      }
    }

    {
      info { "invalid licences" }
      val transporterService = new TransporterServiceStub
      val carrierService = new CarrierServiceStub
      val route: Route = new TransporterRoute(transporterService, carrierService)

      val carriers = CarrierInfo("bob", 33, List("A", "E")) :: Nil
      val transporter = TransporterInfo("moveit", "S1234", List("A59", "Z12", "X44"), carriers)

      transporterService.transporters shouldBe empty
      carrierService.carriers shouldBe empty

      Post("/transporters", transporter) ~> route ~> check {
        status shouldBe BadRequest
        responseAs[String].trim should not be empty
        transporterService.transporters shouldBe empty
        carrierService.carriers shouldBe empty
      }
    }

  }

  it should "listTransporters" in {
    val transporterService = new TransporterServiceStub
    val carrierService = new CarrierServiceStub
    val route: Route = new TransporterRoute(transporterService, carrierService)

    val ciA = CarrierInfo("cr-0", 33, List("A"))
    val ciB = CarrierInfo("cr-1", 18, List("B"))
    val ciAB = CarrierInfo("cr-2", 56, List("A", "B"))

    val tiA = TransporterInfo("tr-0", "001", Nil, ciA :: Nil)
    val tiB = TransporterInfo("tr-1", "002", Nil, ciB :: Nil)
    val tiAB = TransporterInfo("tr-2", "003", Nil, ciAB :: Nil)

    val tis = tiA :: tiB :: tiAB :: Nil

    tis foreach { ti =>
      Post("/transporters", ti) ~> route ~> check {
        status shouldBe OK
        transporterService.transporters should contain(toDomain(ti))
        carrierService.carriers should contain allElementsOf { ti.carriers map (toDomain(_, ti.siret)) }
      }
    }

    info { "unfiltered" }
    Get("/transporters") ~> route ~> check {
      status shouldBe OK
      responseAs[List[TransporterInfo]] should contain allElementsOf tis
    }

    info { "filtered" }
    Get("/transporters?with_licence_type=A") ~> route ~> check {
      status shouldBe OK
      responseAs[List[TransporterInfo]] should contain theSameElementsAs List(tiA, tiAB)
    }

    Get("/transporters?with_licence_type=B") ~> route ~> check {
      status shouldBe OK
      responseAs[List[TransporterInfo]] should contain theSameElementsAs List(tiB, tiAB)
    }

    Get("/transporters?with_licence_type=C") ~> route ~> check {
      status shouldBe OK
      responseAs[List[TransporterInfo]] shouldBe empty
    }
  }

  it should "fail listTransporters" in {
    val transporterService = new TransporterServiceStub
    val carrierService = new CarrierServiceStub
    val route: Route = new TransporterRoute(transporterService, carrierService)

    Get("/transporters?with_licence_type=X") ~> route ~> check {
      status shouldBe BadRequest
      responseAs[String] should not be empty
    }
  }
}
