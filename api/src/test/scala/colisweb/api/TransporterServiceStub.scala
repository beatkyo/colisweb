package colisweb.api

import colisweb.transporter.LicenceType
import colisweb.transporter.Transporter
import colisweb.transporter.TransporterService.CreateTransporterError
import colisweb.transporter.TransporterService

import scala.concurrent.Future
import scala.concurrent.Future.successful

/**
  * For tests only. Synchronized, uniqueness not checked, etc.
  */
class TransporterServiceStub extends TransporterService {

  private var _transporters: List[Transporter] = Nil

  def transporters: List[Transporter] = _transporters

  def listTransporters(licenceFilter: Option[LicenceType]): Future[List[Transporter]] = {
    licenceFilter match {
      case Some(licence) => successful(_transporters filter (_.licences contains licence))
      case None          => successful(_transporters)
    }
  }

  def createTransporter(transporter: Transporter): Future[List[CreateTransporterError]] = {
    this.synchronized { _transporters ::= transporter }

    successful(Nil)
  }

}
