package colisweb.api

import colisweb.carrier.CarrierService
import colisweb.carrier.CarrierService.CreateCarrierError
import colisweb.carrier.{Carrier => DomainCarrier}

import scala.concurrent.Future
import scala.concurrent.Future.successful

/**
  * For tests only. Synchronized, uniqueness not checked, etc.
  */
class CarrierServiceStub extends CarrierService {

  private var _carriers: List[DomainCarrier] = Nil

  def carriers: List[DomainCarrier] = _carriers

  def listCarriersByTransporterSIRET(siret: String): Future[List[DomainCarrier]] = {
    successful(_carriers filter (_.transporterSiret.toLowerCase == siret.toLowerCase))
  }

  def createCarrier(carrier: DomainCarrier): Future[List[CreateCarrierError]] = {
    this.synchronized { _carriers ::= carrier }

    successful(Nil)
  }

}
