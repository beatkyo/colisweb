package colisweb.api
import akka.http.scaladsl.marshalling.GenericMarshallers
import akka.http.scaladsl.model.StatusCodes.BadRequest
import akka.http.scaladsl.model.StatusCodes.InternalServerError
import akka.http.scaladsl.model.StatusCodes.OK
import akka.http.scaladsl.server.Directives
import akka.http.scaladsl.server.Route
import colisweb.api.TransporterRoute.CarrierServiceError
import colisweb.api.TransporterRoute.EmptyCarriersError
import colisweb.api.TransporterRoute.Error
import colisweb.api.TransporterRoute.InvalidLicenceError
import colisweb.api.TransporterRoute.TransporterServiceError
import colisweb.carrier.Carrier
import colisweb.carrier.CarrierService
import colisweb.transporter.LicenceType
import colisweb.transporter.Transporter
import colisweb.transporter.TransporterService

import scala.concurrent.ExecutionContext
import scala.concurrent.Future
import scala.concurrent.Future.sequence
import scala.concurrent.Future.successful
import scala.language.implicitConversions
import scala.util.Failure
import scala.util.Success

class TransporterRoute(transporterService: TransporterService, carrierService: CarrierService)(
    implicit ec: ExecutionContext
) extends Directives
    with JsonSupport
    with ModelMapper
    with GenericMarshallers {

  val createTransporter: Route = {
    path("transporters") {
      post {
        entity(as[TransporterInfo]) { transporterInfo =>
          val carrierInfos = transporterInfo.carriers

          lazy val createDomainTransporter: Future[List[TransporterServiceError]] = {
            transporterService.createTransporter(toDomain(transporterInfo)) map {
              _ map TransporterServiceError
            }
          }

          lazy val createDomainCarriers: Future[List[CarrierServiceError]] = {
            sequence { carrierInfos.map(toDomain(_, transporterInfo.siret)).map(carrierService.createCarrier) } map {
              _.flatten map CarrierServiceError
            }
          }

          lazy val op: Future[List[Error]] = {
            createDomainTransporter flatMap {
              case Nil    => createDomainCarriers
              case errors => successful { errors }
            }
          }

          val validation = {
            val errors = {
              val allowed: String => Boolean = List("A", "B", "C").contains

              List(
                if (carrierInfos.isEmpty) List(EmptyCarriersError(transporterInfo)) else Nil,
                carrierInfos.flatMap { c =>
                  c.licences.filterNot(allowed).map(l => InvalidLicenceError(l, c))
                }
              )
            }

            errors.flatten
          }

          validation match {
            case Nil =>
              onComplete(op) {
                case Success(Nil)    => complete { OK }
                case Success(errors) => complete { InternalServerError -> errors.map(_.message).mkString("\n") }
                case Failure(t)      => complete { InternalServerError -> t }
              }

            case errors =>
              complete { BadRequest -> errors.map(_.message).mkString("\n") }
          }
        }
      }
    }
  }

  val listTransporters: Route = {
    path("transporters") {
      get {
        parameters("with_licence_type".?) { withLicenceType =>
          def withCarriers(transporters: List[Transporter]) = {
            transporters map (t => carrierService.listCarriersByTransporterSIRET(t.siret) map (t -> _))
          }

          def fromDomainList: List[(Transporter, List[Carrier])] => List[TransporterInfo] = {
            _ map { case (t, cs) => fromDomain(t, cs) }
          }

          val validation = {
            val allowed: String => Boolean = List("A", "B", "C").contains

            withLicenceType filterNot allowed map ("Invalid Licence: " + _)
          }

          validation match {
            case None =>
              complete {
                val licenceFilter = withLicenceType map LicenceType.fromString

                transporterService
                  .listTransporters(licenceFilter)
                  .map(withCarriers)
                  .flatMap(sequence(_))
                  .map(fromDomainList)
              }

            case Some(error) =>
              complete { BadRequest -> error }
          }
        }
      }
    }
  }

}

object TransporterRoute extends Directives {
  sealed trait Error { val message: String }
  sealed case class TransporterServiceError(error: TransporterService.Error) extends Error { val message = s"Transporter service error: [ ${error.message} ]" }
  sealed case class CarrierServiceError(error: CarrierService.Error) extends Error { val message = s"Carrier service error: [ ${error.message} ]" }
  sealed case class EmptyCarriersError(transporter: TransporterInfo) extends Error { val message = s"Transporter carriers is empty for $transporter" }
  sealed case class InvalidLicenceError(licence: String, carrier: CarrierInfo) extends Error { val message = s"Invalid licence '$licence' for $carrier" }

  implicit def toRoute(route: TransporterRoute): Route = route.createTransporter ~ route.listTransporters
}
