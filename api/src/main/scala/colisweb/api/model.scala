package colisweb.api

case class TransporterInfo(name: String, siret: String, postalCodes: List[String], carriers: List[CarrierInfo])

case class CarrierInfo(name: String, age: Int, licences: List[String])
