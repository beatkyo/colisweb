package colisweb.api

import colisweb.carrier.Carrier
import colisweb.carrier.{LicenceType => CarrierLicenceType}
import colisweb.transporter.Transporter
import colisweb.transporter.{LicenceType => TransporterLicenceType}

import scala.language.implicitConversions

trait ModelMapper {

  def toDomain(tr: TransporterInfo): Transporter = {
    val licences = tr.carriers.flatMap(_.licences).map(TransporterLicenceType.fromString).distinct

    Transporter(tr.siret, tr.name, tr.postalCodes, licences)
  }

  def toDomain(carrier: CarrierInfo, transporterSiret: String): Carrier = {
    Carrier(transporterSiret, carrier.name, carrier.age, carrier.licences map CarrierLicenceType.fromString)
  }

  def fromDomain(transporter: Transporter, carriers: List[Carrier]): TransporterInfo = {
    TransporterInfo(transporter.name, transporter.siret, transporter.postalCodes, carriers map fromDomain)
  }

  def fromDomain(carrier: Carrier): CarrierInfo = {
    CarrierInfo(carrier.name, carrier.age, carrier.licences map CarrierLicenceType.asString)
  }

}
