package colisweb.api

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import spray.json.DefaultJsonProtocol

trait JsonSupport extends SprayJsonSupport with DefaultJsonProtocol {
  implicit val carrierInfoFormat = jsonFormat3(CarrierInfo)
  implicit val transporterInfoFormat = jsonFormat4(TransporterInfo)
}
