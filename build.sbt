val transporters = project

val carriers = project

val api = project dependsOn (transporters, carriers)

val example = project dependsOn api

publishArtifact := false
