# 3. Design

As the two services are independent from each other the Transporter creation can proceed independently of the carriers creation.

For the carriers creation to eventually succeed, I would reach for some retry logic.
In a real implementation I would build this logic on top of or with a command bus component.
This would provide strong guarantees that the commands will be eventually processed even in the case of a service downtime.
Ultimately, if retrying multiple times over a period still fails, the faulty commands can be published to a 'failure' topic.
This would allow for some manual investigation of the problem.