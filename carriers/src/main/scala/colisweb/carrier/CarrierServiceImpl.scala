package colisweb.carrier

import colisweb.carrier.CarrierService.CreateCarrierError
import colisweb.carrier.CarrierService.EmptyLicencesError
import colisweb.carrier.CarrierService.EmptyNameError
import colisweb.carrier.CarrierService.InvalidAgeError

import scala.concurrent.Future.successful
import scala.concurrent.ExecutionContext
import scala.concurrent.Future

class CarrierServiceImpl(repository: CarrierRepository)(implicit ec: ExecutionContext) extends CarrierService {

  def listCarriersByTransporterSIRET(siret: String): Future[List[Carrier]] = {
    repository.listCarriersByTransporterSIRET(siret)
  }

  def createCarrier(carrier: Carrier): Future[List[CreateCarrierError]] = {
    val validation = {
      val errors = {
        List(
          if (carrier.name.trim.isEmpty) Some(EmptyNameError(carrier)) else None,
          if (carrier.age < 18) Some(InvalidAgeError(carrier)) else None,
          if (carrier.licences.isEmpty) Some(EmptyLicencesError(carrier)) else None
        )
      }

      errors.flatten
    }

    successful(validation) flatMap {
      case Nil    => repository.createCarrier(carrier) map (_ => Nil)
      case errors => successful { errors }
    }
  }

}
