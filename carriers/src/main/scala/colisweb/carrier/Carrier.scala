package colisweb.carrier

case class Carrier(transporterSiret: String, name: String, age: Int, licences: List[LicenceType])
