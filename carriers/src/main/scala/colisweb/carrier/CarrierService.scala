package colisweb.carrier

import colisweb.carrier.CarrierService.CreateCarrierError

import scala.concurrent.Future

trait CarrierService {
  def listCarriersByTransporterSIRET(siret: String): Future[List[Carrier]]

  def createCarrier(carrier: Carrier): Future[List[CreateCarrierError]]
}

object CarrierService {
  sealed trait Error { val message: String }

  sealed trait CreateCarrierError extends Error
  sealed case class EmptyNameError(carrier: Carrier) extends CreateCarrierError { val message = s"Empty name: $carrier" }
  sealed case class InvalidAgeError(carrier: Carrier) extends CreateCarrierError { val message = s"Invalid age: $carrier" }
  sealed case class EmptyLicencesError(carrier: Carrier) extends CreateCarrierError { val message = s"Empty licences: $carrier" }
}
