package colisweb.carrier

sealed trait LicenceType

object LicenceType {
  object A extends LicenceType
  object B extends LicenceType
  object C extends LicenceType

  implicit def fromString: String => LicenceType = {
    case "A" => A
    case "B" => B
    case "C" => C
  }

  implicit def asString: LicenceType => String = {
    case A => "A"
    case B => "B"
    case C => "C"
  }
}
