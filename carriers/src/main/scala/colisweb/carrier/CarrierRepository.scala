package colisweb.carrier
import scala.concurrent.Future

trait CarrierRepository {

  def listCarriersByTransporterSIRET(siret: String): Future[List[Carrier]]

  def createCarrier(carrier: Carrier): Future[Unit]

}
