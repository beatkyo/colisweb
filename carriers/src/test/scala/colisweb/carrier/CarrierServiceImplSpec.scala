package colisweb.carrier
import colisweb.carrier.CarrierService.EmptyLicencesError
import colisweb.carrier.CarrierService.EmptyNameError
import colisweb.carrier.CarrierService.InvalidAgeError
import colisweb.carrier.LicenceType.A
import colisweb.carrier.LicenceType.B
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.EitherValues
import org.scalatest.FlatSpecLike
import org.scalatest.Matchers

import scala.concurrent.ExecutionContext.Implicits.global

class CarrierServiceImplSpec extends FlatSpecLike with Matchers with ScalaFutures with EitherValues {

  behavior of "CarrierServiceImpl"

  it should "createCarrier" in {
    val repository = new CarrierRepositoryStub
    val service = new CarrierServiceImpl(repository)

    val carrier = Carrier("S-0", "bob", 33, List(A, B))

    repository.carriers shouldBe empty
    service.createCarrier(carrier).futureValue shouldBe empty
    repository.carriers should contain(carrier)
  }

  it should "fail createCarrier" in {
    val repository = new CarrierRepositoryStub
    val service = new CarrierServiceImpl(repository)

    val carrier = Carrier("", "", 15, Nil)

    repository.carriers shouldBe empty

    service.createCarrier(carrier).futureValue should contain allOf (
      EmptyNameError(carrier),
      InvalidAgeError(carrier),
      EmptyLicencesError(carrier)
    )

    repository.carriers shouldBe empty
  }

  it should "listCarriersBySIRET" in {
    val repository = new CarrierRepositoryStub
    val service = new CarrierServiceImpl(repository)

    val c0 = Carrier("s-0", "bob", 33, List(A, B))
    val c1 = Carrier("s-1", "max", 25, List(A, B))
    val c2 = Carrier("s-1", "yan", 45, List(A, B))

    repository.carriers shouldBe empty
    service.createCarrier(c0).futureValue shouldBe empty
    service.createCarrier(c1).futureValue shouldBe empty
    service.createCarrier(c2).futureValue shouldBe empty
    repository.carriers should contain theSameElementsAs List(c0, c1, c2)

    service.listCarriersByTransporterSIRET("s-0").futureValue should contain theSameElementsAs List(c0)
    service.listCarriersByTransporterSIRET("s-1").futureValue should contain theSameElementsAs List(c1, c2)
    service.listCarriersByTransporterSIRET("s-whatever").futureValue shouldBe empty
  }

}
