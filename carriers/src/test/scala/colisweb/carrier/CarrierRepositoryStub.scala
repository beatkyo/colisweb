package colisweb.carrier
import scala.concurrent.Future
import scala.concurrent.Future.successful

/**
  * For tests only. Synchronized.
  */
class CarrierRepositoryStub extends CarrierRepository {

  private var _carriers: List[Carrier] = Nil

  def carriers: List[Carrier] = _carriers

  def listCarriersByTransporterSIRET(siret: String): Future[List[Carrier]] = {
    successful(_carriers filter (_.transporterSiret.toLowerCase == siret))
  }

  def createCarrier(carrier: Carrier): Future[Unit] = {
    this.synchronized { _carriers ::= carrier }

    successful(())
  }

}
