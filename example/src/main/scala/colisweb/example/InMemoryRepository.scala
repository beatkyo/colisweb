package colisweb.example
import colisweb.carrier.Carrier
import colisweb.carrier.CarrierRepository
import colisweb.transporter.LicenceType
import colisweb.transporter.Transporter
import colisweb.transporter.TransporterRepository

import scala.concurrent.Future
import scala.concurrent.Future.successful

/**
  * Example purpose. Synchronized.
  */
class InMemoryRepository extends TransporterRepository with CarrierRepository {
  private var transporters: List[Transporter] = Nil
  private var carriers: List[Carrier] = Nil

  def findTransporterBySIRET(siret: String): Future[Option[Transporter]] = {
    successful(transporters find (_.siret.toLowerCase == siret.toLowerCase))
  }

  def listTransportersByLicence(license: LicenceType): Future[List[Transporter]] = {
    successful(transporters filter (_.licences contains license))
  }

  def listTransporters: Future[List[Transporter]] = {
    successful(transporters)
  }

  def createTransporter(transporter: Transporter): Future[Unit] = {
    this.synchronized { transporters ::= transporter }

    successful(())
  }

  def listCarriersByTransporterSIRET(siret: String): Future[List[Carrier]] = {
    successful(carriers filter (_.transporterSiret.toLowerCase == siret.toLowerCase))
  }

  def createCarrier(carrier: Carrier): Future[Unit] = {
    this.synchronized { carriers ::= carrier }

    successful(())
  }

}
