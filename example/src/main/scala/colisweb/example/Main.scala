package colisweb.example
import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Directives
import akka.http.scaladsl.server.Route
import akka.stream.ActorMaterializer
import colisweb.api.TransporterRoute
import colisweb.carrier.CarrierServiceImpl
import colisweb.transporter.TransporterServiceImpl

import scala.io.StdIn

object Main extends Directives {

  def main(args: Array[String]): Unit = {
    //
    // Example inprocess deployment. A real deployment would need to break the hard dependency to Services and Repositories.
    // Adapters would then manage to call them through the network.
    //

    implicit val system = ActorSystem("api")
    implicit val materializer = ActorMaterializer()
    implicit val ec = system.dispatcher

    val repository = new InMemoryRepository
    val transporterService = new TransporterServiceImpl(repository)
    val carrierService = new CarrierServiceImpl(repository)

    val route: Route = new TransporterRoute(transporterService, carrierService)

    val bindingFuture = Http().bindAndHandle(route, "localhost", 8080)

    println(s"Server online at http://localhost:8080/\nPress RETURN to stop...")
    StdIn.readLine() // let it run until user presses return
    bindingFuture
      .flatMap(_.unbind()) // trigger unbinding from the port
      .onComplete(_ => system.terminate()) // and shutdown when done

  }

}
